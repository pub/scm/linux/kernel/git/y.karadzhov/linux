/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_NS_COMMON_H
#define _LINUX_NS_COMMON_H

#include <linux/refcount.h>
#include <linux/idr.h>

struct proc_ns_operations;

struct ns_common {
	struct idr idr;
	atomic_long_t stashed;
	const struct proc_ns_operations *ops;
	unsigned int inum;
	refcount_t count;

#ifdef CONFIG_NAMESPACE_FS
	struct dentry *dentry;
#endif /* CONFIG_NAMESPACE_FS */
};

#endif
